Please run the receive.py file first before running the transmit.py file by typing the following commands into separate terminals:

1. `pip install -r requirements.txt`

2. `python receive.py`

3. `python transmit.py`