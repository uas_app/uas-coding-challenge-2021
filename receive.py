from flask import Flask, request
from math import sqrt
import utm

app = Flask(__name__)

total_distance, total_time = 0, 0
prev_coordinates = None

@app.route("/reset")
def reset():
    global total_distance
    global total_time
    global prev_coordinates
    total_distance, total_time = 0, 0
    prev_coordinates = None

@app.route("/speed", methods=["GET"])
def speed():
    global total_distance
    global total_time
    global prev_coordinates

    latitude = request.args.get("latitude")
    longitude = request.args.get("longitude")
    coordinates = utm.from_latlon(float(latitude), float(longitude))

    if not prev_coordinates: 
        prev_coordinates = coordinates
        return {"average_speed" : 0, "current_speed" : 0}
    if prev_coordinates[2] != coordinates[2] or prev_coordinates[3] != coordinates[3]:
        pass # TODO: calculate distances for coordinates crossing bands/zones
        
    dx, dy = coordinates[0] - prev_coordinates[0], coordinates[1] - prev_coordinates[1]
    distance = sqrt(dx * dx + dy * dy) 
    total_distance += distance
    total_time += 1
    prev_coordinates = coordinates
    return {"average_speed" : total_distance / total_time, "current_speed" : distance}

if __name__ == "__main__":
    app.run()