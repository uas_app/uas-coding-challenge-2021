import requests
import time

URL = "http://127.0.0.1:5000/"

requests.get(url=f"{URL}/reset")

waypoints = open("missionwaypoints.txt", 'r')
for line in waypoints:
    coordinates = [float(coord) for coord in line.split()]
    coordinates = {"latitude": coordinates[0], "longitude": coordinates[1]}
    calculations = requests.get(url=f"{URL}/speed", params=coordinates).json()
    print(f"average speed: {calculations['average_speed']}m/s, current_speed: {calculations['current_speed']}m/s")
    time.sleep(1) # send waypoints every second